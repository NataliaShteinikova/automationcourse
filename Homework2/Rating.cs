﻿using System;
using System.Diagnostics;
using System.Threading;
using FotoWebTestFramework.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Homework2
{
	[TestFixture]
	public class Rating
	{
		private IWebDriver _driver;

		[OneTimeSetUp]
		public void OneTimeSetUp()
		{
			Debug.WriteLine("OneTimeSetUp");
			     
			_driver = new ChromeDriver(@"/Users/nataliashteinikova/Projects/WebDriver30/WebDriver30/");
			_driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
			_driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
			_driver.Navigate().GoToUrl("http:/autotestcourse.fastdev.se");
			_driver.Manage().Window.Maximize();

			{ // login
				_driver.FindElement(By.Id("username-page")).SendKeys("natalia.shteinikova@fastdev.se");
				_driver.FindElement(By.Id("password-page")).SendKeys("test");
				_driver.FindElement(By.Id("page-form-submit")).Click();
			}
		}

		[OneTimeTearDown]
		public void OneTimeTearDown()
		{
			Debug.WriteLine("OneTimeTearDown");

			_driver.FindElement(By.Id("userMenuButton")).Click();
			_driver.FindElement(By.XPath("//li[@id='userMenuButton']//div[@class='userMenuOption logout']")).Click();

			_driver.Quit();
		}

		[SetUp]
		public void SetUp()
		{
			LoginPage loginPage = new LoginPage();

			Debug.WriteLine("SetUp");

			_driver.Navigate().GoToUrl("http://autotestcourse.fastdev.se");
		}

		[TearDown]
		public void TearDown()
		{
			Debug.WriteLine("TearDown");
		}

		[Test]
		public void CheckRating()
		{
			//int a = 3;
			//int b = 7;

			//Assert.AreEqual(3, b, "Numbers a and b have different values");
			//Assert.IsTrue(true, "Comment");
			//Assert.IsFalse(false);

			_driver.FindElement(By.XPath("//a[@href='/fotoweb/archives/5000-Common%20assets/']")).Click();
			var asset = _driver.FindElement(By.XPath("//div[@id='thumbnailWrapper']//a[@href='/fotoweb/archives/5000-Common%20assets/Test%20automation%20course/News%20agencies/2014-05-02T070757Z_1460091317_GM1EA520KIQ01_RTRMADP_3_LEONE-SEACUCUMBERS.jpg.info#c=%2Ffotoweb%2Farchives%2F5000-Common%2520assets%2F']"));
			asset.FindElement(By.XPath("//i[@class='fa icon-fa-check fa-lg pull-right']")).Click();
			WaitForActionPanelIsVisible();
			_driver.FindElement(By.Id("setRatingAction")).Click();
			_driver.FindElement(By.XPath("//button[@data-value=3]")).Click();

			asset.FindElement(By.XPath("//i[@class='fa fa-fw fa-signal icon']")).Click();
			_driver.FindElement(By.XPath("//button[@data-value='1']")).Click();

			asset.Click();

			_driver.SwitchTo().Frame(_driver.FindElement(By.Id("preview-iframe")));

			bool isRate3 = _driver.FindElement(By.XPath("//span[contains(@class, 'rating-info')]")).GetAttribute("class").Contains("rate-3");
			Assert.True(isRate3);
			bool isStatusSet = _driver.FindElement(By.XPath("//span[contains(@class, 'status-1')]")) != null;
			Assert.True(isStatusSet);
		}

		private void WaitForActionPanelIsVisible()
		{
			Thread.Sleep(1000);
		}

		private static void WaitForCondition(Func<bool> condition, string message)
		{
			
		}
	}
}
