﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace WebDriver30
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			IWebDriver driver = new ChromeDriver(@"/Users/nataliashteinikova/Projects/WebDriver30/WebDriver30/");
			try
			{
				driver.Navigate().GoToUrl("http:/autotestcourse.fastdev.se");

				driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
				driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
				driver.Manage().Window.Maximize();

				driver.FindElement(By.Id("username-page")).SendKeys("natalia.shteinikova@fastdev.se");
				driver.FindElement(By.Id("password-page")).SendKeys("test");
				driver.FindElement(By.Id("page-form-submit")).Click();

				//string errorMessage = driver.FindElement(By.XPath("//div[@class='form-item error-message form-group']")).Text;

				//isEqual("Wrong user name or password", errorMessage, "Something went not as expected");

				driver.FindElement(By.XPath("//a[@href='/fotoweb/archives/5000-Common%20assets/']")).Click();
				var asset = driver.FindElement(By.XPath("//a[@href='/fotoweb/archives/5000-Common%20assets/Test%20automation%20course/News%20agencies/2014-05-02T070757Z_1460091317_GM1EA520KIQ01_RTRMADP_3_LEONE-SEACUCUMBERS.jpg.info#c=%2Ffotoweb%2Farchives%2F5000-Common%2520assets%2F']"));
				asset.FindElement(By.XPath("//i[@class='fa icon-fa-check fa-lg pull-right']")).Click();

				driver.FindElement(By.XPath("//div[@class='actions-wrap']")).FindElement(By.XPath("//i[@class='fa fa-fw fa-star icon']")).Click();
				driver.FindElement(By.XPath("//div[@class='popover fade top in']")).FindElement(By.XPath("//button[@data-value='3']")).Click();
				asset.Click();

			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				driver.Quit();
			}
		}

		static void isEqual(string expected, string actual, string message)
		{
			if (expected != actual)
				throw new Exception(message);
		}
	}
}