﻿using FotoWebTestFramework.DriverController;
using FotoWebTestFramework.Pages.PageElements;
using OpenQA.Selenium;

namespace FotoWebTestFramework.Pages
{
	public class LoginPage
	{
		private IWebDriver _driver;
		public LoginPage()
		{
			_driver = WebDriver.GetInstance();
		}

		public EditBox LoginOrEmail
		{
			get
			{
				return new EditBox(_driver, By.Id("username-page"));
			}
		}

		public EditBox Password
		{
			get
			{
				return new EditBox(_driver, By.Id("password-page"));
			}
		}

		public void Login()
		{
			new Button(_driver, By.Id("password-page")).Click();
		}
	}
}
