﻿//using System.Object;
using System.Void;
using OpenQA.Selenium;

namespace FotoWebTestFramework.Pages.PageElements
{
	public class EditBox
	{
		private readonly IWebDriver _driver;
		private readonly By _locator;

		internal EditBox(IWebDriver webDriver, By locator)
		{
			_driver = webDriver;
			_locator = locator;
		}

		public void KeyPress(string value)
		{
			_driver.FindElement(_locator).SendKeys(value);
		}

		public void Clear()
		{
			_driver.FindElement(_locator).Clear();
		}

		public void ClearAndKeyPress(string value)
		{
			Clear();
			KeyPress(value);
		}
	}
}
