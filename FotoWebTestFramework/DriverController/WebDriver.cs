﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace FotoWebTestFramework.DriverController
{
	public static class WebDriver
	{
		private static IWebDriver _instance;
		public static IWebDriver GetInstance()
		{
			if (_instance == null)
			{
				_instance = new ChromeDriver(@"C:\Work\Lessons\WebDriver30\packages\Selenium.WebDriver.ChromeDriver.2.28.0\driver");
				_instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
				_instance.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
				_instance.Manage().Window.Maximize();
				_instance.Navigate().GoToUrl("http://autotestcourse.fastdev.se");
			}

			return _instance;
		}
	}
}
